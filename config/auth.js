/**
 * Created by julian on 18/7/16.
 */

module.exports = {
    'local' : {
        'clientID': 'enter client id here',
        'clientSecret': '757470c8ee069313ebe2cacbddfdf721',
        'expiresIn': 86400, //seconds
        'saltRounds': 10,
        'callbackURL': 'enter callback here'
    },

    'facebook' : {
        'clientID': 'enter client id here',
        'clientSecret': 'enter client secret here',
        'callbackURL': 'enter callback here'
    },

    'google' : {
        'clientID': 'enter client id here',
        'clientSecret': 'enter client secret here',
        'callbackURL': 'enter callback here'
    }

};