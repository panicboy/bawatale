/**
 * Created by julian on 16/7/16.
 */

'use strict';

module.exports = {
    /* CREATE ATTRIBUTES */
    validFieldsToCreate : [
        'title',
        'description',
        'notes',
        'category',
        'phase',
        'configuration.private',
        'configuration.private_group'
    ],

    requiredFieldsToCreate : [
        'title',
        'category'
    ],

    /* UPDATE ATTRIBUTES */

    validFieldsToUpdate : [
        '_id',
        'phase',
        'description',
        'notes',
        'category'
    ],


    requiredFieldsToUpdate : [
        '_id'
    ],

    /* SHOW ATTRIBUTES */

    validFieldsToShow : [
        '_id'
    ],


    requiredFieldsToShow : [
        '_id'
    ],

    /* FIELD TYPES ATTRIBUTES */

    fieldTypes : {
        '_id': ['required', '_id'],
        'category': ['tinyInt', 'required'],
        'phase': ['tinyInt', 'required'],
        'title': ['required', 'smallText'],
        'description': ['mediumText'],
        'notes': ['mediumText']
    }
};
