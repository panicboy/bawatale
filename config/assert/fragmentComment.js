/**
 * Created by julian on 16/7/16.
 */

'use strict';

module.exports = {
    /* CREATE ATTRIBUTES */
    validFieldsToCreate : [
        'fragment_id',
        'post_id',
        'parent_id',
        'content.text'
    ],

    requiredFieldsToCreate : [
        'fragment_id',
        'post_id',
        'content.text'
    ],

    /* UPDATE ATTRIBUTES */

    validFieldsToUpdate : [
        '_id',
        'content.text'
    ],


    requiredFieldsToUpdate : [
        '_id'
    ],

    /* SHOW ATTRIBUTES */

    validFieldsToShow : [
        '_id'
    ],


    requiredFieldsToShow : [
        '_id'
    ],

    /* FIELD TYPES ATTRIBUTES */

    fieldTypes : {
        '_id': ['required', '_id'],
        'fragment_id': ['required', '_id'],
        'post_id': ['required', '_id'],
        'parent_id': ['required', '_id'],
        'content.text': ['required', 'ultraText']
    }
};
