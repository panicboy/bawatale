/**
 * Created by julian on 16/7/16.
 */

'use strict';

module.exports = {
    /* CREATE ATTRIBUTES */
    validFieldsToCreate : [
        'post_id',
        'content.text',
        'title',
        'notes'
    ],

    requiredFieldsToCreate : [
        'post_id',
        'content.text',
        'title'
    ],

    /* UPDATE ATTRIBUTES */

    validFieldsToUpdate : [
        '_id',
        'title',
        'notes'
    ],


    requiredFieldsToUpdate : [
        '_id'
    ],


    /* SHOW ATTRIBUTES */

    validFieldsToShow : [
        '_id',
        'post_id'
    ],


    requiredFieldsToShow : [
        '_id',
        'post_id'
    ],

    /* FIELD TYPES ATTRIBUTES */

    fieldTypes : {
        '_id': ['required', '_id'],
        'post_id': ['required', '_id'],
        'content.text': ['required', 'ultraText'],
        'category': ['tinyInt', 'required'],
        'title': ['required', 'smallText'],
        'description': ['mediumText'],
        'notes': ['mediumText']
    }
};
