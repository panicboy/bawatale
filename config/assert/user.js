/**
 * Created by julian on 16/7/16.
 */

'use strict';

module.exports = {

    /* CREATE ATTRIBUTES */
    validFieldsToCreate : [
        'email',
        'password',
        'name'
    ],

    requiredFieldsToCreate : [
        'email',
        'password',
        'name'
    ],


    /* UPDATE ATTRIBUTES */

    validFieldsToUpdate : [
        'profile.firstname',
        'profile.surname',
        'profile.city',
        'profile.country',
        'profile.gender',
        'profile.birthday'
    ],

    requiredFieldsToUpdate : [],

    /* SHOW ATTRIBUTES */

    validFieldsToShow : [
        '_id'
    ],


    requiredFieldsToShow : [
        '_id'
    ],

    /* FIELD TYPES ATTRIBUTES */

    fieldTypes : {
        '_id': ['required', '_id'],
        'email': ['required', 'email', 'smallText'],
        'name': ['required', 'alias'],
        'password': ['required', 'password'],
        'profile.firstname': ['smallText'],
        'profile.surname': ['smallText'],
        'profile.city': ['smallText'],
        'profile.country': ['smallText'],
        'profile.birthday': ['shortDate'],
        'profile.gender': ['boolean']
    }
};
