/**
 * Created by julian on 16/7/16.
 */

'use strict';

module.exports = {
    /* CREATE ATTRIBUTES */
    validFieldsToCreate : [
        'profile_id',
        'profile_comment_id'
    ],

    requiredFieldsToCreate : [
        'profile_id',
        'profile_comment_id'
    ],

    /* UPDATE ATTRIBUTES */

    validFieldsToUpdate : [
        '_id',
    ],


    requiredFieldsToUpdate : [
        '_id'
    ],

    /* REMOVE ATTRIBUTES */

    validFieldsToRemove : [
        '_id',
    ],


    requiredFieldsToRemove : [
        '_id'
    ],

    /* SHOW ATTRIBUTES */

    validFieldsToShow : [
        '_id'
    ],


    requiredFieldsToShow : [
        '_id'
    ],

    /* FIELD TYPES ATTRIBUTES */

    fieldTypes : {
        '_id': ['required', '_id'],
        'profile_id': ['required', '_id'],
        'profile_comment_id': ['required', '_id'],
        'user_id': ['required', '_id']
    }
};
