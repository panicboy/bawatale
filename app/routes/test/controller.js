// Import dependencies
const passport = require('passport');
const express = require('express');
const jwt = require('jsonwebtoken');
let User = require('../../model/user');
let TestRepository = require('../../repository/test');

let router = express.Router();

// Export the routes for our app to use
module.exports = function(app) {

    // API Route Section

    // Initialize passport for use
    app.use(passport.initialize());



    // Register new user
    router.get('/promise', function(req, res, next) {
        TestRepository.asyncPromisesTest().then(function() {
            return bw.send(res, 200);
        });
    });

    // Register new user
    router.get('/async', function(req, res, next) {
        TestRepository.asyncModuleTest(function() {
            return bw.send(res, 200);
        });
    });

    // Register new user
    router.get('/sync', function(req, res, next) {
        TestRepository.syncPromisesTest(function() {
            return bw.send(res, 200);
        });
    });

    return router;
};


//module.exports = router;
