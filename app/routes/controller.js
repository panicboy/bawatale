// Import dependencies
let passport = require('passport');
let express = require('express');
let jwt = require('jsonwebtoken');

// Set up middleware
const requireAuth = passport.authenticate('jwt', { session: false });
let router = express.Router();

// Export the routes for our app to use
module.exports = function(app) {

    // API Route Section

    // Initialize passport for use
    app.use(passport.initialize());

    // Bring in defined Passport Strategy
    bw.cfg.passport(passport);

    router.use('/' + bw.cfg.main.privateUri, requireAuth, require('./private/controller')(app));
    router.use('/' + bw.cfg.main.defaultUri + '/auth', require('./auth/controller')(app));
    router.use('/test', require('./test/controller')(app));

    /* GET API STATUS. */
    router.get('/', function(req, res, next) {
        //res.render('index', { title: 'Express' });
        /*res.status(200);
        res.json({message: 'OK'});*/
        bw.send(res, 200, 'OK');
    });

    /* GET API ABOUT. */
    router.get('/about', function(req, res, next) {
        /*res.status(200);
        res.json({message: 'Learn about us'});*/
        bw.send(res, 200, 'Learn about us');

    });

    return router;
};
