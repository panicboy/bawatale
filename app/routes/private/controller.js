// Import dependencies
let express = require('express');
let mongoose = require('mongoose');
let passport = require('passport');
let bwRequestPost = require('../../../lib/middleware/bwRequestPost')('post_id');
let bwRequestFragment = require('../../../lib/middleware/bwRequestFragment')('fragment_id');
let bwRequestProfile = require('../../../lib/middleware/bwRequestProfile')('profile_id');
let jwt = require('jsonwebtoken');
let User = require('../../model/user');
let router = express.Router();

// Export the routes for our app to use
module.exports = function(app) {

  // API Route Section

  // Initialize passport for use
  app.use(passport.initialize());

  // Register new users
  /* GET private home page. */
  router.get('/', function(req, res, next) {
    //res.render('index', { title: 'Express' });
    console.log('It worked! User id is: ' + req.user._id);
    User.find({}, function(err, users) {
      if(!err) {
        bw.send(res, 200, bw.msg.LISTING, users);
      }
    });
  });

  router.use(bwRequestPost.grantShortByField);
  router.use(bwRequestFragment.grantShortByField);
  router.use(bwRequestProfile.grantShortByField);
  router.use('/user', require('./user/controller')(app));
  router.use('/post', require('./post/controller')(app));
  router.use('/fragment', require('./fragment/controller')(app));


  return router;
};


//module.exports = router;
