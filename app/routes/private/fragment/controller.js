// Import dependencies
const passport = require('passport');
const express = require('express');
const jwt = require('jsonwebtoken');
let FragmentRepository = require('../../../repository/fragment');
let FragmentCommentRepository = require('../../../repository/fragmentComment');
let FragmentLikeRepository = require('../../../repository/fragmentLike');
let FragmentCommentLikeRepository = require('../../../repository/fragmentCommentLike');
let bwRequestFragment = require('../../../../lib/middleware/bwRequestFragment')('_id');

let router = express.Router();

// Export the routes for our app to use
module.exports = function(app) {

    // API Route Section

    // Initialize passport for use
    app.use(passport.initialize());

    //COMMENTS

    router.post('/comment/create', function(req, res) {
        let repository = new FragmentCommentRepository(req.body, req.user._id, req.bwpost, req.bwfragment);
        repository.create()
            .then(function(result) {
                bw.sendResult(res,result);
            })
            .catch(function(err) {
                bw.sendResult(res,err);
            });
    });

    router.put('/comment/update', function(req, res) {
        let repository = new FragmentCommentRepository(req.body, req.user._id);
        repository.update()
            .then(function(result) {
                bw.sendResult(res,result);
            })
            .catch(function(err) {
                bw.sendResult(res,err);
            });
    });


    // COMMENT LIKES

    router.post('/comment/like/create', function(req, res) {
        let repository = new FragmentCommentLikeRepository(req.body, req.user._id, req.bwpost, req.bwfragment);
        repository.create()
            .then(function(result) {
                bw.sendResult(res,result);
            })
            .catch(function(err) {
                bw.sendResult(res,err);
            });
    });

    router.delete('/comment/like/remove', function(req, res) {
        let repository = new FragmentCommentLikeRepository(req.body, req.user._id);
        repository.remove()
            .then(function(result) {
                bw.sendResult(res,result);
            })
            .catch(function(err) {
                bw.sendResult(res,err);
            });
    });

    // LIKES

    router.post('/like/create', function(req, res) {
        let repository = new FragmentLikeRepository(req.body, req.user._id, req.bwpost, req.bwfragment);
        repository.create()
            .then(function(result) {
                bw.sendResult(res,result);
            })
            .catch(function(err) {
                bw.sendResult(res,err);
            });
    });

    router.delete('/like/remove', function(req, res) {
        let repository = new FragmentLikeRepository(req.body, req.user._id);
        repository.remove()
            .then(function(result) {
                bw.sendResult(res,result);
            })
            .catch(function(err) {
                bw.sendResult(res,err);
            });
    });

    // Register new fragment
    router.post('/create', function(req, res) {
        let repository = new FragmentRepository(req.body, req.user._id, req.bwpost);
        repository.create()
            .then(function(result) {
                bw.sendResult(res,result);
            })
            .catch(function(err) {
                bw.sendResult(res,err);
            });
    });

    //router.use('/update', bwRequestFragment.grantShortByField);

    router.put('/update', function(req, res) {
        let repository = new FragmentRepository(req.body, req.user._id);
        repository.update()
            .then(function(result) {
                bw.sendResult(res,result);
            })
            .catch(function(err) {
                bw.sendResult(res,err);
            });
    });


    router.use('/', bwRequestFragment.grantByField);

    router.post('/', function(req, res) {
        let repository = new FragmentRepository(req.body, req.user._id, req.bwpost, req.bwfragment);
        repository.show()
            .then(function(result) {
                bw.sendResult(res,result);
            })
            .catch(function(err) {
                bw.sendResult(res,err);
            });
    });


    return router;
};


//module.exports = router;
