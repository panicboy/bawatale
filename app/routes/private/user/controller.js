// Import dependencies
const passport = require('passport');
const express = require('express');
const jwt = require('jsonwebtoken');
let User = require('../../../model/user');
let UserRepository = require('../../../repository/user');
let ProfileCommentRepository = require('../../../repository/profileComment');
let ProfileCommentLikeRepository = require('../../../repository/profileCommentLike');
let router = express.Router();

// Export the routes for our app to use
module.exports = function(app) {
    // API Route Section

    // Initialize passport for use
    app.use(passport.initialize());

    //COMMENTS

    router.post('/comment/create', function(req, res) {
        let repository = new ProfileCommentRepository(req.body, req.user._id, req.bwprofile);
        repository.create()
            .then(function(result) {
                bw.sendResult(res,result);
            })
            .catch(function(err) {
                bw.sendResult(res,err);
            });
    });

    router.put('/comment/update', function(req, res) {
        let repository = new ProfileCommentRepository(req.body, req.user._id);
        repository.update()
            .then(function(result) {
                bw.sendResult(res,result);
            })
            .catch(function(err) {
                bw.sendResult(res,err);
            });
    });

    // COMMENT LIKES

    router.post('/comment/like/create', function(req, res) {
        let repository = new ProfileCommentLikeRepository(req.body, req.user._id, req.bwpost);
        repository.create()
            .then(function(result) {
                bw.sendResult(res,result);
            })
            .catch(function(err) {
                bw.sendResult(res,err);
            });
    });


    router.delete('/comment/like/remove', function(req, res) {
        let repository = new ProfileCommentLikeRepository(req.body, req.user._id);
        repository.remove()
            .then(function(result) {
                bw.sendResult(res,result);
            })
            .catch(function(err) {
                bw.sendResult(res,err);
            });
    });

    router.put('/update', function(req, res) {
        let repository = new UserRepository(req.body, req.user._id);
        repository.update()
            .then(function(result) {
              bw.sendResult(res,result);
            })
            .catch(function(err) {
              bw.sendResult(res,err);
            });
    });

  return router;
};


//module.exports = router;
