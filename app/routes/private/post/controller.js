// Import dependencies
let passport = require('passport');
let express = require('express');
let jwt = require('jsonwebtoken');
let PostRepository = require('../../../repository/post');
let PostLikeRepository = require('../../../repository/postLike');
let PostCommentRepository = require('../../../repository/postComment');
let PostCommentLikeRepository = require('../../../repository/postCommentLike');
let bwRequestPost = require('../../../../lib/middleware/bwRequestPost')('_id');

let router = express.Router();

// Export the routes for our app to use
module.exports = function(app) {

    // API Route Section

    // Initialize passport for use
    app.use(passport.initialize());

    //COMMENTS

    router.post('/comment/create', function(req, res) {
        let repository = new PostCommentRepository(req.body, req.user._id, req.bwpost);
        repository.create()
            .then(function(result) {
                bw.sendResult(res,result);
            })
            .catch(function(err) {
                bw.sendResult(res,err);
            });
    });

    router.put('/comment/update', function(req, res) {
        let repository = new PostCommentRepository(req.body, req.user._id);
        repository.update()
            .then(function(result) {
                bw.sendResult(res,result);
            })
            .catch(function(err) {
                bw.sendResult(res,err);
            });
    });

    // LIKES

    router.post('/like/create', function(req, res) {
        let repository = new PostLikeRepository(req.body, req.user._id, req.bwpost);
        repository.create()
            .then(function(result) {
                bw.sendResult(res,result);
            })
            .catch(function(err) {
                bw.sendResult(res,err);
            });
    });

    router.delete('/like/remove', function(req, res) {
        let repository = new PostLikeRepository(req.body, req.user._id, req.bwpost);
        repository.remove()
            .then(function(result) {
                bw.sendResult(res,result);
            })
            .catch(function(err) {
                bw.sendResult(res,err);
            });
    });

    // COMMENT LIKES

    router.post('/comment/like/create', function(req, res) {
        let repository = new PostCommentLikeRepository(req.body, req.user._id, req.bwpost);
        repository.create()
            .then(function(result) {
                bw.sendResult(res,result);
            })
            .catch(function(err) {
                bw.sendResult(res,err);
            });
    });


    router.delete('/comment/like/remove', function(req, res) {
        let repository = new PostCommentLikeRepository(req.body, req.user._id);
        repository.remove()
            .then(function(result) {
                bw.sendResult(res,result);
            })
            .catch(function(err) {
                bw.sendResult(res,err);
            });
    });



    // POSTS

    // Register new post
    router.post('/create', function(req, res) {
        let repository = new PostRepository(req.body, req.user._id);
        repository.create()
            .then(function(result) {
                bw.sendResult(res,result);
            })
            .catch(function(err) {
                bw.sendResult(res,err);
            });
    });



    //router.use('/update', bwRequestPost.grantShortByField);

    router.put('/update', function(req, res) {
        let repository = new PostRepository(req.body, req.user._id);
        repository.update()
            .then(function(result) {
                bw.sendResult(res,result);
            })
            .catch(function(err) {
                bw.sendResult(res,err);
            });
    });

    router.use('/', bwRequestPost.grantByField);

    router.post('/', function(req, res) {
        let repository = new PostRepository(req.body, req.user._id, req.bwpost);
        repository.show()
            .then(function(result) {
                bw.sendResult(res,result);
            })
            .catch(function(err) {
                bw.sendResult(res,err);
            });
    });


    return router;
};


//module.exports = router;