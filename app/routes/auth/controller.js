// Import dependencies
const passport = require('passport');
const express = require('express');
const jwt = require('jsonwebtoken');
let User = require('../../model/user');
let UserRepository = require('../../repository/user');

let router = express.Router();

// Export the routes for our app to use
module.exports = function(app) {

    // API Route Section

    // Initialize passport for use
    app.use(passport.initialize());

    // Register new user
    router.post('/user/create', function(req, res) {
        let repository = new UserRepository(req.body);
        repository.create()
        .then(function(result) {
            bw.sendResult(res,result);
        })
        .catch(function(err) {
            bw.sendResult(res,err);
        });
    });

    // Authenticate the user and get a JSON Web Token to include in the header of future requests.
    router.post('/authenticate', function(req, res) {
        User.findOne({
            email: req.body.email
        }, function(err, user) {
            if (err) {
                throw err;
            }

            if (!user) {
                bw.send(res, 401, bw.msg.AUTH_FAILED);
            } else {
                // Check if password matches
                user.comparePassword(req.body.password, function(err, isMatch) {
                    if (isMatch && !err) {
                        // Create token if the password matched and no error was thrown
                        let token = jwt.sign(user, bw.cfg.auth.local.clientSecret, {
                            expiresIn: bw.cfg.auth.local.expiresIn // in seconds
                        });

                        res.status(200).json({ success: true, token: 'JWT ' + token });
                    } else {
                        bw.send(res, 401, bw.msg.AUTH_FAILED);
                    }
                });
            }
        });
    });

    return router;
};


//module.exports = router;
