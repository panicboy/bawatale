/**
 * Created by julian on 16/7/16.
 */

'use strict';

let mongoose = require('mongoose');
let bcrypt = require('bcrypt');
const collectionModel = 'User';
const collectionName = 'user';
const authConfig = require('../../config/auth');

// Schema defines how the user's data will be stored in MongoDB
let schema = new mongoose.Schema({
    email: {
        type: String,
        lowercase: true,
        trim: true,
        unique: true,
        required: true
    },
    name: { type: String, required: false },
    password: { type: String, required: true },
    created_at: { type: Date, default: Date.now },
    updated_at: { type: Date, default: Date.now },
    confirmed_at: { type: Date, default: null },
    platform: { type: String, default: 'local'},
    lang: { type: String, default: 'es-ES'},
    role: {
        type: String,
        enum: ['client', 'manager', 'admin'],
        default: 'client'
    },
    local: {
        id: String,
        token: String
    },
    facebook: {
        id: String,
        token: String
    },
    twitter: {
        id: String,
        token: String
    },
    profile: {
        firstname: { type: String, default: null },
        surname: { type: String, default: null },
        city: { type: String, default: null },
        country: { type: String, default: null },
        birthday: { type: Date, default: null },
        gender: { type: Boolean, default: null },
    },
    meta: {
        posts: { type: Number, default: 0 },
        fragments: { type: Number, default: 0 },
        hits: { type: Number, default: 0 },
        real_views: { type: Number, default: 0 },
        followers: { type: Number, default: 0 },
        chosen_fragments: { type: Number, default: 0 },
        comments: { type: Number, default: 0 },
        profile_comments: { type: Number, default: 0 }
    }
}, { collection: collectionName });

// Saves the user's password hashed (plain text password storage is not good)
schema.pre('save', function (next) {
    let user = this;
    if (this.isModified('password') || this.isNew) {
        bcrypt.genSalt(authConfig.local.saltRounds, function (err, salt) {
            if (err) {
                return next(err);
            }
            bcrypt.hash(user.password, salt, function(err, hash) {
                if (err) {
                    return next(err);
                }
                user.password = hash;
                next();
            });
        });
    } else {
        return next();
    }
});

// Create method to compare password input to password saved in database
schema.methods.comparePassword = function(pw, cb) {
    bcrypt.compare(pw, this.password, function(err, isMatch) {
        if (err) {
            return cb(err);
        }
        cb(null, isMatch);
    });
};

module.exports = mongoose.model(collectionModel, schema);