/**
 * Created by julian on 16/7/16.
 */

'use strict';

let mongoose = require('mongoose');
const collectionModel = 'ProfileCommentLike';
const collectionName = 'profileCommentLike';

// Schema defines how the profile comment like's data will be stored in MongoDB
let schema = new mongoose.Schema({
    profile_id: { type: mongoose.Schema.Types.ObjectId, required: true },
    profile_comment_id: { type: mongoose.Schema.Types.ObjectId, required: true },
    user_id: { type: mongoose.Schema.Types.ObjectId, required: true },
    created_at: { type: Date, default: Date.now },
}, { collection: collectionName });

module.exports = mongoose.model(collectionModel, schema);
