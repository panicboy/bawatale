/**
 * Created by julian on 16/7/16.
 */

'use strict';

let mongoose = require('mongoose');
const collectionModel = 'ProfileComment';
const collectionName = 'profileComment';

// Schema defines how the user's data will be stored in MongoDB
let schema = new mongoose.Schema({
    parent_id: { type: mongoose.Schema.Types.ObjectId, default: null },
    profile_id: { type: mongoose.Schema.Types.ObjectId, required: true },
    user_id: { type: mongoose.Schema.Types.ObjectId, required: true },
    content: {
        text: { type: String, required: true }
    },
    created_at: { type: Date, default: Date.now },
    published_at: { type: Date, default: Date.now },
    updated_at: { type: Date, default: Date.now },
    meta: {
        likes: { type: Number, default: 0 },
        comments: { type: Number, default: 0 }
    }
}, { collection: collectionName });

module.exports = mongoose.model(collectionModel, schema);