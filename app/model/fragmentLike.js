/**
 * Created by julian on 16/7/16.
 */

'use strict';

let mongoose = require('mongoose');
const collectionModel = 'fragmentLike';
const collectionName = 'fragmentLike';

// Schema defines how the user's data will be stored in MongoDB
let schema = new mongoose.Schema({
    fragment_id: { type: mongoose.Schema.Types.ObjectId, required: true },
    user_id: { type: mongoose.Schema.Types.ObjectId, required: true },
    created_at: { type: Date, default: Date.now },
}, { collection: collectionName });

module.exports = mongoose.model(collectionModel, schema);


