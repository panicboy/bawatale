/**
 * Created by julian on 16/7/16.
 */

'use strict';

let mongoose = require('mongoose');
const collectionModel = 'Fragment';
const collectionName = 'fragment';

// Schema defines how the user's data will be stored in MongoDB
let schema = new mongoose.Schema({
    post_id: { type: mongoose.Schema.Types.ObjectId, required: true },
    user_id: { type: mongoose.Schema.Types.ObjectId, required: true },
    phase: { type: Number, default: 0, required: true },
    instant: { type: Number, default: 0, required: true },
    title: { type: String, required: true }, /* Unused */
    description: { type: String, default: null },
    notes: { type: String, default: null },
    configuration: {},
    content: {
        text: { type: String, required: true },
    },
    category: Number, /* Unused, inherit from Post, TODO: it can be good to take stadistics */
    is_chosen: Number,
    created_at: { type: Date, default: Date.now },
    published_at: { type: Date, default: Date.now },
    updated_at: { type: Date, default: Date.now },
    meta: {
        likes: { type: Number, default: 0 },
        hits: { type: Number, default: 0 }, /* Unused */
        real_views: { type: Number, default: 0 }, /* Unused */
        comments: { type: Number, default: 0 }
    }
}, { collection: collectionName });

module.exports = mongoose.model(collectionModel, schema);

