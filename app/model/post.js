/**
 * Created by julian on 16/7/16.
 */

'use strict';

let mongoose = require('mongoose');
const collectionModel = 'Post';
const collectionName = 'post';

// Schema defines how the user's data will be stored in MongoDB
let schema = new mongoose.Schema({
    parent_id: Number,
    user_id: { type: mongoose.Schema.Types.ObjectId, required: true },
    phase: { type: Number, default: 0 },
    instant: { type: Number, default: 0 },
    title: { type: String, required: true },
    description: { type: String, default: null },
    notes: { type: String, default: null },
    private: { type:Boolean, default: false },
    configuration: {
        private: { type: Boolean, default: false },
        private_group: { type: Array, default: [] }
    },
    content: {
        text: String
    },
    category: Number,
    is_dynamic: { type: Boolean, default: false },
    created_at: { type: Date, default: Date.now },
    published_at: { type: Date, default: Date.now },
    updated_at: { type: Date, default: Date.now },
    lang: { type: String, default: 'es-ES'},
    meta: {
        likes: { type: Number, default: 0 },
        hits: { type: Number, default: 0 },
        real_views: { type: Number, default: 0 },
        archived: { type: Number, default: 0 },
        fragments: { type: Number, default: 0 },
        chosen_fragments: { type: Number, default: 0 },
        comments: { type: Number, default: 0 }
    }
}, { collection: collectionName });

module.exports = mongoose.model(collectionModel, schema);
