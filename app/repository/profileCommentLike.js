/**
 * Created by julian on 16/7/16.
 */

'use strict';

const ProfileCommentLike = require('../model/profileCommentLike');
const profileCommentLikeType = require('../../config/assert/profileCommentLike');
const ProfileComment = require('../model/profileComment');
//const User = require('../model/user');
const ProfileCommentRepository = require('../../app/repository/profileComment');
const BaseRepository = require('./_baseRepository');
let async = require('async');

// Schema defines how the po'ts data will be stored in MongoDB
let ProfileCommentLikeRepository = function(body, user_id, post) {
    BaseRepository.call(this, ProfileCommentLike, profileCommentLikeType);
    this.body = body;
    this.user_id = user_id || null;
    //this.validFieldsToUpdate = BaseRepository.exclusionPath(this.fields, ['__v', 'email', 'password']);
    this._init();
};

ProfileCommentLikeRepository.prototype = Object.create(BaseRepository.prototype);
ProfileCommentLikeRepository.prototype.constructor = ProfileCommentLikeRepository;

ProfileCommentLikeRepository.prototype.create = function() {
    let self = this;
    return new Promise(function(resolve, reject) {
        if (!self.checkFields({isCreating: true})) {
            reject(bw.generateResult(400, bw.msg.FILL_REQUIRED_FIELDS));
            return;
        }

        let data = self.getDataFromBody();

        if (data === null || self.user_id === null) {
            reject(bw.generateResult(400, bw.msg.FILL_REQUIRED_FIELDS));
        }

        /** Prevention post_id is equal to  self.post._id **/
        data.user_id = self.user_id;

        let promiseStack = [];

        let dataProfileComment = {
            'query': {
                '_id': data.profile_comment_id,
                'profile_id': data.profile_id
            },
            'fields': {
                '_id': 1,
                'parent_id': 1,
                'user_id': 1,
                'profile_id': 1
            },
            'getOne': true
        };

        let profileCommentRepository = new ProfileCommentRepository(null, null);

        promiseStack.push(profileCommentRepository.get(dataProfileComment));
        promiseStack.push(self.notExistsProfileCommentLike());
        promiseStack.push(function(result, callback) {
            setImmediate(function () {
                const newLike = new ProfileCommentLike(data);

                newLike.save(function (err) {
                    if (err) {
                        return callback(bw.generateResult(500, bw.msg.CREATE_ERROR, err));
                    }

                    // Todo: it can be in the user repository like an action like increasePosts

                    ProfileComment.update(
                        {'_id': data.profile_comment_id},
                        {'$inc': {'meta.likes': 1}},
                        function(err,num) {}
                    );

                    callback(null, bw.generateResult(201, bw.msg.ELEMENT_CREATED));
                });
            });
        });

        /* Todo: Check if it is an open post in another case check if I am in the valid users */
        async.waterfall(promiseStack, function (err, result) {
            if(err) {
                return reject(err);
            }

            resolve(result);
        });
    });
};


ProfileCommentLikeRepository.prototype.remove = function() {
    let self = this;
    return new Promise(function(resolve, reject) {
        if (!self.checkFields({isDeleting: true})) {
            reject(bw.generateResult(400, bw.msg.FILL_REQUIRED_FIELDS));
            return;
        }

        let data = self.getDataFromBody();

        if (data === null || self.user_id === null) {
            reject(bw.generateResult(400, bw.msg.FILL_REQUIRED_FIELDS));
        }

        /** Prevention post_id is equal to  self.post._id **/
        data.user_id = self.user_id;

        ProfileCommentLike.findOneAndRemove(data, function(err, document){
            if (err) {
                return reject(bw.generateResult(500, bw.msg.REMOVE_ERROR));
            }

            if (!document) {
                return reject(bw.generateResult(404, bw.msg.ELEMENT_NOT_FOUND));
            }

            ProfileComment.update(
                {'_id': document.profile_comment_id},
                {'$inc': {'meta.likes': -1}},
                function(err,num) {});
            console.log(document);

            resolve(bw.generateResult(200, bw.msg.ELEMENT_REMOVED));
        });
    });
};




ProfileCommentLikeRepository.prototype.notExistsProfileCommentLike = function() {
    let self = this;
    return function(result, callback) {
        setImmediate(function () {

            let data = {};
            data.profile_comment_id = result._id;
            data.user_id = self.user_id;


            ProfileCommentLike.find(data, function (err, profileCommentLikes) {
                if (err) {
                    return callback(bw.generateResult(500, bw.msg.SEARCH_ERROR));
                }
                if (profileCommentLikes.length) {
                    return callback(bw.generateResult(404, bw.msg.ELEMENT_ALREADY_EXISTS));
                }

                callback(null, true);
            });
        });
    };
};


module.exports = ProfileCommentLikeRepository;
