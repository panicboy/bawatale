/**
 * Created by julian on 16/7/16.
 */

'use strict';

let async = require('async');

// Schema defines how the user's data will be stored in MongoDB
let TestRepository = function() {};


TestRepository.asyncPromisesTest = function() {

    let divisibleBy = 10;
    let promiseStack = [];

    let fs = require('fs');

    fs.writeFile('./myfile.txt', '');
    promiseStack.push(new Promise(function(resolve, reject) {
        resolve(bw.generateResult(200, 'Finish 3'));
        for (let i = 0; i < 1000000000; i++) {
            console.console.log(i);
        }
        fs.appendFile('./myfile.txt', 'Finish 1\n');
    }));

    promiseStack.push(new Promise(function(resolve, reject) {
        resolve(bw.generateResult(200, 'Finish 3'));
        for (let i = 0; i < 1000; i++) {
            console.console.log(i);
        }
        fs.appendFile('./myfile.txt', 'Finish 2\n');

    }));

    promiseStack.push(new Promise(function(resolve, reject) {
        resolve(bw.generateResult(200, 'Finish 3'));
        for (let i = 0; i < 1000; i++) {
            console.console.log(i);
        }
        fs.appendFile('./myfile.txt', 'Finish 3\n');
    }));


    return Promise.all(promiseStack);
};


TestRepository.syncPromisesTest = function(finalCallback) {

    let divisibleBy = 10;

    let p3 = function() {
        for (let i = 0; i < 1000; i++) {
            if ((i+1) % divisibleBy === 0) {
                console.info('3 .....', i + 1);
            }
        }
        finalCallback();
    };

    let p2 = function() {
        for (let i = 0; i < 1000; i++) {
            if ((i+1) % divisibleBy === 0) {
                console.info('2 .....', i + 1);
            }
        }
        p3();
    };

    let p1 = function() {
        for (let i = 0; i < 1000; i++) {
            if ((i+1) % divisibleBy === 0) {
                console.info('1 .....', i + 1);
            }
        }
        p2();
    };

    p1();
};





TestRepository.asyncModuleTest = function(finalCallback) {

    let divisibleBy = 10;
    let asyncStack = [];


    let fs = require('fs');

    fs.writeFile('./myfile.txt', '');
    asyncStack.push(function(callback) {
        setImmediate(function(){
            for (let i = 0; i < 1000000000; i++) {
                console.console.log(i);
            }
            fs.appendFile('./myfile.txt', 'Finish 0\n');
            //callback(null, bw.generateResult(200, 'Finish 1'));
        });

    });

    asyncStack.push(function(callback) {
        setImmediate(function(){
            for (let i = 0; i < 1000; i++) {
                console.console.log(i);
            }
            fs.appendFile('./myfile.txt', 'Finish 2\n');
            //callback(null, bw.generateResult(200, 'Finish 2'));
        });
    });

    asyncStack.push(function(callback) {
        setImmediate(function(){
            for (let i = 0; i < 1000; i++) {
                console.console.log(i);
            }
            fs.appendFile('./myfile.txt', 'Finish 3\n');
            //callback(null, bw.generateResult(200, 'Finish 3'));
        });
    });



    return async.parallel(asyncStack, finalCallback);
};





module.exports = TestRepository;
