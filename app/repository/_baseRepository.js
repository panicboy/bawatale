/**
 * Created by julian on 16/7/16.
 */

'use strict';

let BwCheck = require('../../lib/utils/BwCheck');

// Parent repository class
let BaseRepository = function(model, modelType) {
    this.model = model;
    this.modelType = modelType;
};

BaseRepository.exclusionPath = function (paths, excluded) {
    let fields = [];
    if (!BwCheck.isArray(excluded)) {
        return;
    }

    for (let i = 0; i < paths.length; i++) {
        if (excluded.indexOf(paths[i]) < 0) {
            fields.push(paths[i]);
        }
    }

    return fields;
};

BaseRepository.prototype.checkFields = function({isCreating = false, isUpdating = false,
    isShowing = false, isDeleting = false}) {
    console.log('BaseRepository.prototype.checkFields');

    let currentPaths;
    let requiredFields;

    if(!isCreating && !isUpdating && !isShowing && !isDeleting) {
        return false;
    }

    if (isCreating) {
        currentPaths = this.validFieldsToCreate;
        requiredFields = this.requiredFieldsToCreate;
    } else if (isUpdating) {
        currentPaths = this.validFieldsToUpdate;
        requiredFields = this.requiredFieldsToUpdate;
    } else if (isShowing) {
        currentPaths = this.validFieldsToShow;
        requiredFields = this.requiredFieldsToShow;
    } else if (isDeleting) {
        currentPaths = this.validFieldsToRemove;
        requiredFields = this.requiredFieldsToRemove;
    }


    if (!this._checkBody(currentPaths)) {
        return false;
    }

    for (let i = 0; i < currentPaths.length; i++) {
        if (typeof this.body[currentPaths[i]] === 'undefined' && requiredFields.indexOf(currentPaths[i]) > -1) {
            return false;
        }

        if( typeof this.body[currentPaths[i]] !== 'undefined') {
            if (!BwCheck.validate(this.fieldTypes[currentPaths[i]] || [], this.body[currentPaths[i]])) {
                return false;
            }
        }

    }

    return true;
};

BaseRepository.prototype.getDataFromBody = function(excluded) {
    let self = this;
    let data = {};
    excluded = excluded || [];

    let counter = 0;
    Object.keys(this.body).forEach(function(key) {
        if(excluded.indexOf(key) === -1) {
            data[key] = self.body[key].trim();
            counter++;
        }
    });

    return counter ? data : null;
};



BaseRepository.prototype._checkBody = function(paths) {

    function isInValidPaths(element, index, array) {
        console.log(element, 'IN', paths);
        return paths.indexOf(element) > -1;
    }

    return Object.keys(this.body).every(isInValidPaths);
};

BaseRepository.prototype._init = function () {
    this.fields = Object.keys(this.model.schema.paths);
    this.validFieldsToCreate = this.modelType.validFieldsToCreate || null;
    this.requiredFieldsToCreate = this.modelType.requiredFieldsToCreate || null;
    this.validFieldsToUpdate = this.modelType.validFieldsToUpdate || null;
    this.requiredFieldsToUpdate = this.modelType.requiredFieldsToUpdate || null;
    this.validFieldsToShow = this.modelType.validFieldsToShow || null;
    this.requiredFieldsToShow = this.modelType.requiredFieldsToShow || null;
    this.validFieldsToRemove = this.modelType.validFieldsToRemove || null;
    this.requiredFieldsToRemove = this.modelType.requiredFieldsToShow || null;
    this.fieldTypes = this.modelType.fieldTypes || null;
};


BaseRepository.prototype.get = function({
    query = null,
    fields = null,
    page = null,
    limit = null,
    sort = null,
    getOne = false,
    notFoundError = true,
}) {
    let self = this;
    return function(callback) {
        setImmediate(function () {
            if (query === null || !(Object.keys(query).length)) {
                return callback(bw.generateResult(400, bw.msg.SEARCH_ERROR));
            }

            if (fields !== null && !(Object.keys(fields).length)) {
                return callback(bw.generateResult(400, bw.msg.SEARCH_ERROR));
            }

            if (getOne === true) {
                limit = 1;
            }

            let qry = self.model.find(query);

            if (fields !== null) {
                qry.select(fields);
            }

            if (page !== null && limit !== null) {
                qry.skip((page-1) * limit);
            }

            if (limit !== null) {
                qry.limit(limit);
            }

            if (sort !== null) {
                qry.sort(sort);
            }

            qry.exec(function (err, doc) {
                if (err) {
                    return callback(bw.generateResult(400, bw.msg.SEARCH_ERROR));
                }

                if (notFoundError && !doc.length) {
                    return callback(bw.generateResult(404, bw.msg.ELEMENT_NOT_FOUND));
                }

                if (getOne) {
                    return callback(null, doc[0]);
                }

                callback(null, doc);
            });
        });
    };
};


module.exports = BaseRepository;
