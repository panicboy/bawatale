/**
 * Created by julian on 16/7/16.
 */

'use strict';

const Fragment = require('../model/fragment');
const fragmentType = require('../../config/assert/fragment');
const Post = require('../model/post');
const User = require('../model/user');
const BaseRepository = require('./_baseRepository');
let async = require('async');

// Schema defines how the po'ts data will be stored in MongoDB
let FragmentRepository = function(body, user_id, post, fragment) {
    BaseRepository.call(this, Fragment, fragmentType);
    this.body = body;
    this.user_id = user_id || null;
    this.post = post || null;
    this.fragment= fragment || null;
    //this.validFieldsToUpdate = BaseRepository.exclusionPath(this.fields, ['__v', 'email', 'password']);
    this._init();
};

FragmentRepository.prototype = Object.create(BaseRepository.prototype);
FragmentRepository.prototype.constructor = FragmentRepository;

FragmentRepository.prototype.create = function() {
    let self = this;
    return new Promise(function(resolve, reject) {
        if (!self.checkFields({isCreating: true})) {
            reject(bw.generateResult(400, bw.msg.FILL_REQUIRED_FIELDS));
            return;
        }

        let dataExists = {};

        let data = self.getDataFromBody();

        if (data === null || self.user_id === null) {
            reject(bw.generateResult(400, bw.msg.FILL_REQUIRED_FIELDS));
        }

        let promiseStack = [];

        promiseStack.push(self.exists(dataExists));
        promiseStack.push(function(result, callback) {
            setImmediate(function () {

                data.phase = self.post.phase;
                data.instant = self.post.instant;
                data.user_id = self.user_id;

                const newFragment = new Fragment(data);

                newFragment.save(function (err) {
                    if (err) {
                        return callback(bw.generateResult(500, bw.msg.CREATE_ERROR));
                    }

                    // Todo: it can be in the user repository like an action like increasePosts
                    User.update({'_id': self.user_id}, {'$inc': {'meta.fragments': 1}}, function(err,num) {});
                    Post.update({'_id': self.post._id}, {'$inc': {'meta.fragments': 1}}, function(err,num) {});


                    callback(null, bw.generateResult(201, bw.msg.ELEMENT_CREATED));
                });
            });
        });

        /* Todo: Check if it is an open post in another case check if I am in the valid users */
        async.waterfall(promiseStack, function (err, result) {
            if(err) {
                return reject(err);
            }

            resolve(result);
        });


    });
};

FragmentRepository.prototype.update = function() {
    let self = this;
    return new Promise(function(resolve, reject) {
        if (!self.checkFields({isUpdating: true})) {
            reject(bw.generateResult(400, bw.msg.FILL_REQUIRED_FIELDS));
            return;
        }

        let query = {'_id': self.body._id, user_id: self.user_id};
        let excluded = ['_id'];
        let data = self.getDataFromBody(excluded);


        if (data === null || self.user_id === null) {
            reject(bw.generateResult(400, bw.msg.FILL_REQUIRED_FIELDS));
        }

        data.updated_at = Date.now();

        Fragment.findOneAndUpdate(query, data, {upsert: false}, function(err, doc){
            if (!doc) {
                return reject(bw.generateResult(404, bw.msg.ELEMENT_NOT_FOUND));
            }

            if (err) {
                return reject(bw.generateResult(500, bw.msg.UPDATE_ERROR));
            }

            resolve(bw.generateResult(201, bw.msg.ELEMENT_UPDATED));
        });

    });
};

FragmentRepository.prototype.show = function() {
    let self = this;
    return new Promise(function(resolve, reject) {
        if (!self.checkFields({isShowing: true})) {
            reject(bw.generateResult(400, bw.msg.FILL_REQUIRED_FIELDS));
            return;
        }

        let data = self.getDataFromBody();
        /** Prevention post_id is equal to  self.post._id **/
        /** Prevention _id is equal to  self.fragment._id **/
        data._id = self.fragment._id;
        data.post_id = self.post._id;

        // ToDo: use async parallel to get the comments (limited by page) and the fragments (limited by page)
        // ToDo: use async parallel to put the hits in the post it can do using find and update directly
        Fragment.update(data, {'$inc': {'meta.hits': 1}}, function(err,num) {});

        console.log('dataShow',data);
        let result = {};
        result.post = self.post;
        result.fragment = self.fragment;
        result.fragment.meta.hits++;
        result.comments = [];

        resolve(bw.generateResult(200, bw.msg.LISTING, result));
    });
};


FragmentRepository.prototype.exists = function(data) {
    let self = this;
    return function(callback) {
        setImmediate(function () {
            data.user_id = self.user_id;
            data.post_id = self.post._id;
            data.instant = self.post.instant;

            Fragment.find(data, function (err, fragments) {
                if (err) {
                    return callback(bw.generateResult(500, bw.msg.SEARCH_ERROR));
                }
                if (fragments.length) {
                    return callback(bw.generateResult(400, bw.msg.ELEMENT_ALREADY_EXISTS));
                }

                callback(null, false);
            });
        });
    };
};

module.exports = FragmentRepository;
