/**
 * Created by julian on 16/7/16.
 */

'use strict';

const Post = require('../model/post');
const postType = require('../../config/assert/post');
const User = require('../model/user');
const BaseRepository = require('./_baseRepository');
let async = require('async');

// Schema defines how the po'ts data will be stored in MongoDB
let PostRepository = function(body, user_id, post) {
    BaseRepository.call(this, Post, postType);
    this.body = body;
    this.user_id = user_id || null;
    this.post = post || null;
    //this.validFieldsToUpdate = BaseRepository.exclusionPath(this.fields, ['__v', 'email', 'password']);
    this._init();
};

PostRepository.prototype = Object.create(BaseRepository.prototype);
PostRepository.prototype.constructor = PostRepository;

PostRepository.prototype.create = function() {
    let self = this;
    return new Promise(function(resolve, reject) {
        if (!self.checkFields({isCreating: true})) {
            reject(bw.generateResult(400, bw.msg.FILL_REQUIRED_FIELDS));
            return;
        }

        let preventDate = new Date();
        preventDate.setSeconds(preventDate.getSeconds() - 10); // last 10 seconds
        let dataExists = {
            'user_id': self.user_id,
            'created_at': {
                '$gte': preventDate
            }
            //'title': self.body.title.trim(),
        };


        let promiseStack = [];

        promiseStack.push(self.exists(dataExists));

        async.series(promiseStack, function(err, result) {
            // results is now equal to: {one: 1, two: 2}
            if(err) {
                return reject(err);
            }

            let data = self.getDataFromBody();

            if (data === null || self.user_id === null) {
                reject(bw.generateResult(400, bw.msg.FILL_REQUIRED_FIELDS));
            }

            data.user_id = self.user_id;

            const newPost = new Post(data);

            newPost.save(function (err) {
                if (err) {
                    return reject(bw.generateResult(500, bw.msg.CREATE_ERROR));
                }

                // ToDo: it can be in the user repository like an action like increasePosts
                User.update({'_id': self.user_id}, {'$inc': {'meta.posts': 1}}, function(err,num) {});


                resolve(bw.generateResult(201, bw.msg.ELEMENT_CREATED));
            });
        });

    });
};

PostRepository.prototype.update = function() {
    let self = this;
    return new Promise(function(resolve, reject) {
        if (!self.checkFields({isUpdating: true})) {
            reject(bw.generateResult(400, bw.msg.FILL_REQUIRED_FIELDS));
            return;
        }

        //console.log('updating', self.post);
        let query = {'_id': self.body.post_id, user_id: self.user_id};
        let excluded = ['_id'];
        let data = self.getDataFromBody(excluded);


        if (data === null || self.user_id === null) {
            reject(bw.generateResult(400, bw.msg.FILL_REQUIRED_FIELDS));
        }

        data.updated_at = Date.now();

        Post.findOneAndUpdate(query, data, {upsert: false}, function(err, doc){
            if (err) {
                return reject(bw.generateResult(500, bw.msg.UPDATE_ERROR));
            }

            if (!doc) {
                return reject(bw.generateResult(404, bw.msg.ELEMENT_NOT_FOUND));
            }

            resolve(bw.generateResult(201, bw.msg.ELEMENT_UPDATED));
        });

    });
};

PostRepository.prototype.show = function() {
    let self = this;
    return new Promise(function(resolve, reject) {
        if (!self.checkFields({isShowing: true})) {
            reject(bw.generateResult(400, bw.msg.FILL_REQUIRED_FIELDS));
            return;
        }

        let data = self.getDataFromBody();
        /** Prevention _id is equal to  self.post._id **/
        data._id = self.post._id;

        // ToDo: use async parallel to get the comments (limited by page) and the fragments (limited by page)
        // ToDo: use async parallel to put the hits in the post it can do using find and update directly
        Post.update(data, {'$inc': {'meta.hits': 1}}, function(err,num) {});

        let result = {};
        result.post = self.post;
        result.post.meta.hits++;
        result.fragments = [];
        result.comments = [];

        resolve(bw.generateResult(200, bw.msg.LISTING, result));
    });
};

PostRepository.prototype.exists = function(data) {
    return function(callback) {
        setImmediate(function () {
            Post.find(data, function (err, posts) {
                if (err) {
                    return callback(bw.generateResult(500,  bw.msg.SEARCH_ERROR), null);
                }

                if (posts.length) {
                    return callback(bw.generateResult(400,  bw.msg.ELEMENT_ALREADY_EXISTS), null);
                }

                callback(null, false);
            });
        });
    };
};

module.exports = PostRepository;
