/**
 * Created by julian on 16/7/16.
 */

'use strict';

const PostLike = require('../model/postLike');
const postLikeType = require('../../config/assert/postLike');
const Post = require('../model/post');
//const User = require('../model/user');
const BaseRepository = require('./_baseRepository');
let async = require('async');

// Schema defines how the po'ts data will be stored in MongoDB
let PostLikeRepository = function(body, user_id, post) {
    BaseRepository.call(this, PostLike, postLikeType);
    this.body = body;
    this.user_id = user_id || null;
    this.post = post || null;
    //this.validFieldsToUpdate = BaseRepository.exclusionPath(this.fields, ['__v', 'email', 'password']);
    this._init();
};

PostLikeRepository.prototype = Object.create(BaseRepository.prototype);
PostLikeRepository.prototype.constructor = PostLikeRepository;

PostLikeRepository.prototype.create = function() {
    let self = this;
    return new Promise(function(resolve, reject) {
        if (!self.checkFields({isCreating: true})) {
            reject(bw.generateResult(400, bw.msg.FILL_REQUIRED_FIELDS));
            return;
        }

        let data = self.getDataFromBody();

        if (data === null || self.user_id === null || self.post=== null) {
            reject(bw.generateResult(400, bw.msg.FILL_REQUIRED_FIELDS));
        }

        /** Prevention post_id is equal to  self.post._id **/
        data.post_id = self.post._id;
        data.user_id = self.user_id;

        let promiseStack = [];

        promiseStack.push(self.notExistsPostLike());
        promiseStack.push(function(result, callback) {
            setImmediate(function () {
                const newLike = new PostLike(data);

                newLike.save(function (err) {
                    if (err) {
                        return callback(bw.generateResult(500, bw.msg.CREATE_ERROR, err));
                    }

                    // Todo: it can be in the user repository like an action like increasePosts

                    Post.update({'_id': self.post._id}, {'$inc': {'meta.likes': 1}}, function(err,num) {});

                    callback(null, bw.generateResult(201, bw.msg.ELEMENT_CREATED));
                });
            });
        });

        /* Todo: Check if it is an open post in another case check if I am in the valid users */
        async.waterfall(promiseStack, function (err, result) {
            if(err) {
                return reject(err);
            }

            resolve(result);
        });
    });
};


PostLikeRepository.prototype.remove = function() {
    let self = this;
    return new Promise(function(resolve, reject) {
        if (!self.checkFields({isDeleting: true})) {
            reject(bw.generateResult(400, bw.msg.FILL_REQUIRED_FIELDS));
            return;
        }

        let data = self.getDataFromBody();

        if (data === null || self.user_id === null) {
            reject(bw.generateResult(400, bw.msg.FILL_REQUIRED_FIELDS));
        }

        /** Prevention post_id is equal to  self.post._id **/
        data.user_id = self.user_id;

        PostLike.findOneAndRemove(data, function(err, document){
            if (err) {
                return reject(bw.generateResult(500, bw.msg.REMOVE_ERROR));
            }

            if (!document) {
                return reject(bw.generateResult(404, bw.msg.ELEMENT_NOT_FOUND));
            }

            Post.update({'_id': document.post_id}, {'$inc': {'meta.likes': -1}}, function(err,num) {});

            console.log(document);

            resolve(bw.generateResult(200, bw.msg.ELEMENT_REMOVED));
        });
    });
};




PostLikeRepository.prototype.notExistsPostLike = function() {
    let self = this;
    return function(callback) {
        setImmediate(function () {

            let data = {};
            data.post_id = self.post._id;
            data.user_id = self.user_id;


            PostLike.find(data, function (err, postLikes) {
                if (err) {
                    return callback(bw.generateResult(500, bw.msg.REMOVE_ERROR));
                }
                if (postLikes.length) {
                    return callback(bw.generateResult(404, bw.msg.ELEMENT_ALREADY_EXISTS));
                }

                callback(null, true);
            });
        });
    };
};


module.exports = PostLikeRepository;
