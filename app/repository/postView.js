/**
 * Created by julian on 16/7/16.
 */

'use strict';

//TODO do a generic save function to create ir update only date it will be using in the show post method outer here

const PostView = require('../model/postView');
const postViewType = require('../../config/assert/postView');
const Post = require('../model/post');
//const User = require('../model/user');
const BaseRepository = require('./_baseRepository');
let async = require('async');

// Schema defines how the po'ts data will be stored in MongoDB
let PostViewRepository = function(body, user_id, post) {
    BaseRepository.call(this, PostView, postViewType);
    this.body = body;
    this.user_id = user_id || null;
    this.post = post || null;
    //this.validFieldsToUpdate = BaseRepository.exclusionPath(this.fields, ['__v', 'email', 'password']);
    this._init();
};

PostViewRepository.prototype = Object.create(BaseRepository.prototype);
PostViewRepository.prototype.constructor = PostViewRepository;

PostViewRepository.prototype.create = function() {
    let self = this;
    return new Promise(function(resolve, reject) {
        if (!self.checkFields({isCreating: true})) {
            reject(bw.generateResult(400, bw.msg.FILL_REQUIRED_FIELDS));
            return;
        }

        let data = self.getDataFromBody();

        if (data === null || self.user_id === null || self.post=== null) {
            reject(bw.generateResult(400, bw.msg.FILL_REQUIRED_FIELDS));
        }

        /** Prevention post_id is equal to  self.post._id **/
        data.post_id = self.post._id;
        data.user_id = self.user_id;

        let promiseStack = [];

        promiseStack.push(self.notExistsPostLike());
        promiseStack.push(function(result, callback) {
            setImmediate(function () {
                const newLike = new PostLike(data);

                newLike.save(function (err) {
                    if (err) {
                        return callback(bw.generateResult(500, bw.msg.CREATE_ERROR, err));
                    }

                    // Todo: it can be in the user repository like an action like increasePosts

                    Post.update({'_id': self.post._id}, {'$inc': {'meta.likes': 1}}, function(err,num) {});

                    callback(null, bw.generateResult(201, bw.msg.ELEMENT_CREATED));
                });
            });
        });

        /* Todo: Check if it is an open post in another case check if I am in the valid users */
        async.waterfall(promiseStack, function (err, result) {
            if(err) {
                return reject(err);
            }

            resolve(result);
        });
    });
};

PostViewRepository.prototype.save = function() {

};

PostLikeRepository.prototype.notExistsPostLike = function() {
    let self = this;
    return function(callback) {
        setImmediate(function () {

            let data = {};
            data.post_id = self.post._id;
            data.user_id = self.user_id;


            PostLike.find(data, function (err, postLikes) {
                if (err) {
                    return callback(bw.generateResult(500, bw.msg.REMOVE_ERROR));
                }
                if (postLikes.length) {
                    return callback(bw.generateResult(404, bw.msg.ELEMENT_ALREADY_EXISTS));
                }

                callback(null, true);
            });
        });
    };
};

module.exports = PostViewRepository;
