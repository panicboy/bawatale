/**
 * Created by julian on 16/7/16.
 */

'use strict';

const User = require('../model/user');
const userType = require('../../config/assert/user');
const BaseRepository = require('./_baseRepository');
let async = require('async');

// Schema defines how the user's data will be stored in MongoDB
let UserRepository = function(body, user_id, profile) {
    BaseRepository.call(this, User, userType);
    this.body = body;
    this.user_id = user_id || null;
    this.profile = profile || null;
    //this.validFieldsToUpdate = BaseRepository.exclusionPath(this.fields, ['__v', 'email', 'password']);
    this._init();
};

UserRepository.prototype = Object.create(BaseRepository.prototype);
UserRepository.prototype.constructor = UserRepository;

UserRepository.prototype.create = function() {
    let self = this;
    return new Promise(function(resolve, reject) {

        if (!self.checkFields({isCreating: true})) {
            reject(bw.generateResult(400, bw.msg.FILL_REQUIRED_FIELDS));
            return;
        }

        let dataEmailExists = {'email': self.body.email.trim()};
        let dataNameExists = {'name': self.body.name.trim()};
        let promiseStack = [];

        promiseStack.push(self.exists(dataEmailExists));
        promiseStack.push(self.exists(dataNameExists));

        async.series(promiseStack, function(err, results) {
            // results is now equal to: {one: 1, two: 2}
            if(err) {
                return reject(err);
            }

            let data = self.getDataFromBody();

            const newUser = new User(data);

            newUser.save(function (err) {
                if (err) {
                    reject(bw.generateResult(500, bw.msg.CREATE_ERROR));
                    return;
                }

                resolve(bw.generateResult(201, bw.msg.ELEMENT_CREATED));
            });
        });

    });
};

UserRepository.prototype.update = function() {
    let self = this;
    return new Promise(function(resolve, reject) {
        if (!self.checkFields({isUpdating: true})) {
            reject(bw.generateResult(400, bw.msg.FILL_REQUIRED_FIELDS));
            return;
        }

        let query = {'_id': self.user_id};
        let excluded = ['_id'];
        let data = self.getDataFromBody(excluded);

        if (data === null || self.user_id === null) {
            reject(bw.generateResult(400, bw.msg.FILL_REQUIRED_FIELDS));
        }

        data.updated_at = Date.now();

        User.findOneAndUpdate(query, data, {upsert: false}, function(err, doc){
            if (!doc) {
                return reject(bw.generateResult(404, bw.msg.ELEMENT_NOT_FOUND));
            }

            if (err) {
                return reject(bw.generateResult(500, bw.msg.UPDATE_ERROR));
            }

            resolve(bw.generateResult(201, bw.msg.ELEMENT_UPDATED));
        });

    });
};

UserRepository.prototype.exists = function(data) {
    return function(callback) {
        setImmediate(function () {
            User.find(data, function (err, users) {
                if (!err && users.length) {
                    let message = typeof data.email === 'undefined' ?
                    bw.msg.NAME_ALREADY_EXISTS :
                    bw.msg.ADDRESS_ALREADY_EXISTS;
                    return callback(bw.generateResult(400, message), null);
                }

                callback(null, true);
            });
        });
    };
};

module.exports = UserRepository;
