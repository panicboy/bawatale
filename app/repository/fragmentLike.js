/**
 * Created by julian on 16/7/16.
 */

'use strict';

const FragmentLike = require('../model/fragmentLike');
const fragmentLikeType = require('../../config/assert/fragmentLike');
const Fragment = require('../model/fragment');
//const User = require('../model/user');
const BaseRepository = require('./_baseRepository');
let async = require('async');

// Schema defines how the po'ts data will be stored in MongoDB
let FragmentLikeRepository = function(body, user_id, post, fragment) {
    BaseRepository.call(this, FragmentLike, fragmentLikeType);
    this.body = body;
    this.user_id = user_id || null;
    this.post = post || null;
    this.fragment= fragment || null;
    //this.validFieldsToUpdate = BaseRepository.exclusionPath(this.fields, ['__v', 'email', 'password']);
    this._init();
};

FragmentLikeRepository.prototype = Object.create(BaseRepository.prototype);
FragmentLikeRepository.prototype.constructor = FragmentLikeRepository;

FragmentLikeRepository.prototype.create = function() {
    let self = this;
    return new Promise(function(resolve, reject) {
        if (!self.checkFields({isCreating: true})) {
            reject(bw.generateResult(400, bw.msg.FILL_REQUIRED_FIELDS));
            return;
        }

        let data = self.getDataFromBody();

        if (data === null || self.user_id === null || self.post=== null || self.fragment=== null) {
            reject(bw.generateResult(400, bw.msg.FILL_REQUIRED_FIELDS));
        }

        /** Prevention fragment_id is equal to  self.fragment._id **/
        data.fragment_id = self.fragment._id;
        data.user_id = self.user_id;

        let promiseStack = [];

        promiseStack.push(self.notExistsFragmentLike());
        promiseStack.push(function(result, callback) {
            setImmediate(function () {
                const newComment = new FragmentLike(data);

                newComment.save(function (err) {
                    if (err) {
                        return callback(bw.generateResult(500, bw.msg.CREATE_ERROR, err));
                    }

                    // Todo: it can be in the user repository like an action like increasePosts

                    Fragment.update({'_id': self.fragment._id}, {'$inc': {'meta.likes': 1}}, function(err,num) {});

                    callback(null, bw.generateResult(201, bw.msg.ELEMENT_CREATED));
                });
            });
        });

        /* Todo: Check if it is an open post in another case check if I am in the valid users */
        async.waterfall(promiseStack, function (err, result) {
            if(err) {
                return reject(err);
            }

            resolve(result);
        });
    });
};


FragmentLikeRepository.prototype.remove = function() {
    let self = this;
    return new Promise(function(resolve, reject) {
        if (!self.checkFields({isDeleting: true})) {
            reject(bw.generateResult(400, bw.msg.FILL_REQUIRED_FIELDS));
            return;
        }

        let data = self.getDataFromBody();

        if (data === null || self.user_id === null) {
            reject(bw.generateResult(400, bw.msg.FILL_REQUIRED_FIELDS));
        }

        data.user_id = self.user_id;

        FragmentLike.findOneAndRemove(data, function(err, document){
            if (err) {
                return reject(bw.generateResult(500, bw.msg.REMOVE_ERROR));
            }

            if (!document) {
                return reject(bw.generateResult(404, bw.msg.ELEMENT_NOT_FOUND));
            }

            Fragment.update({'_id': document.fragment_id}, {'$inc': {'meta.likes': -1}}, function(err,num) {});
            console.log(document);

            resolve(bw.generateResult(200, bw.msg.ELEMENT_REMOVED));
        });
    });
};






FragmentLikeRepository.prototype.notExistsFragmentLike = function() {
    let self = this;
    return function(callback) {
        setImmediate(function () {

            let data = {};
            data.fragment_id = self.fragment._id;
            data.user_id = self.user_id;


            FragmentLike.find(data, function (err, fragmentLikes) {
                if (err) {
                    return callback(bw.generateResult(500, bw.msg.REMOVE_ERROR));
                }
                if (fragmentLikes.length) {
                    return callback(bw.generateResult(404, bw.msg.ELEMENT_ALREADY_EXISTS));
                }

                callback(null, true);
            });
        });
    };
};


module.exports = FragmentLikeRepository;
