/**
 * Created by julian on 16/7/16.
 */

'use strict';

const FragmentCommentLike = require('../model/fragmentCommentLike');
const fragmentCommentLikeType = require('../../config/assert/fragmentCommentLike');
const FragmentComment = require('../model/fragmentComment');
//const User = require('../model/user');
const FragmentCommentRepository = require('../../app/repository/fragmentComment');
const BaseRepository = require('./_baseRepository');
let async = require('async');

// Schema defines how the po'ts data will be stored in MongoDB
let FragmentCommentLikeRepository = function(body, user_id, post, fragment) {
    BaseRepository.call(this, FragmentCommentLike, fragmentCommentLikeType);
    this.body = body;
    this.user_id = user_id || null;
    this.post = post || null;
    this.fragment = fragment || null;
    //this.validFieldsToUpdate = BaseRepository.exclusionPath(this.fields, ['__v', 'email', 'password']);
    this._init();
};

FragmentCommentLikeRepository.prototype = Object.create(BaseRepository.prototype);
FragmentCommentLikeRepository.prototype.constructor = FragmentCommentLikeRepository;

FragmentCommentLikeRepository.prototype.create = function() {
    let self = this;
    return new Promise(function(resolve, reject) {
        if (!self.checkFields({isCreating: true})) {
            reject(bw.generateResult(400, bw.msg.FILL_REQUIRED_FIELDS));
            return;
        }

        let data = self.getDataFromBody();

        if (data === null || self.user_id === null) {
            reject(bw.generateResult(400, bw.msg.FILL_REQUIRED_FIELDS));
        }

        /** Prevention post_id is equal to  self.post._id **/
        data.user_id = self.user_id;
        data.post_id = self.post._id;
        data.fragment_id = self.fragment._id;

        let promiseStack = [];

        let dataFragmentComment = {
            'query': {
                '_id': data.fragment_comment_id,
                'post_id': data.post_id,
                'fragment_id': data.fragment_id
            },
            'fields': {
                '_id': 1,
                'parent_id': 1,
                'user_id': 1,
                'fragment_id': 1,
                'post_id': 1
            },
            'getOne': true
        };


        let fragmentCommentRepository = new FragmentCommentRepository(null, null);

        promiseStack.push(fragmentCommentRepository.get(dataFragmentComment));
        promiseStack.push(self.notExistsFragmentCommentLike());
        promiseStack.push(function(result, callback) {
            setImmediate(function () {
                const newLike = new FragmentCommentLike(data);

                newLike.save(function (err) {
                    if (err) {
                        return callback(bw.generateResult(500, bw.msg.CREATE_ERROR, err));
                    }

                    // Todo: it can be in the user repository like an action like increasePosts

                    FragmentComment.update(
                        {'_id': data.fragment_comment_id},
                        {'$inc': {'meta.likes': 1}},
                        function(err, num) {}
                    );

                    callback(null, bw.generateResult(201, bw.msg.ELEMENT_CREATED));
                });
            });
        });

        /* Todo: Check if it is an open post in another case check if I am in the valid users */
        async.waterfall(promiseStack, function (err, result) {
            if(err) {
                return reject(err);
            }

            resolve(result);
        });
    });
};


FragmentCommentLikeRepository.prototype.remove = function() {
    let self = this;
    return new Promise(function(resolve, reject) {
        if (!self.checkFields({isDeleting: true})) {
            reject(bw.generateResult(400, bw.msg.FILL_REQUIRED_FIELDS));
            return;
        }

        let data = self.getDataFromBody();

        if (data === null || self.user_id === null) {
            reject(bw.generateResult(400, bw.msg.FILL_REQUIRED_FIELDS));
        }

        /** Prevention post_id is equal to  self.post._id **/
        data.user_id = self.user_id;

        FragmentCommentLike.findOneAndRemove(data, function(err, document){
            if (err) {
                return reject(bw.generateResult(500, bw.msg.REMOVE_ERROR));
            }

            if (!document) {
                return reject(bw.generateResult(404, bw.msg.ELEMENT_NOT_FOUND));
            }

            FragmentComment.update(
                {'_id': document.fragment_comment_id},
                {'$inc': {'meta.likes': -1}},
                function(err,num) {}
            );

            console.log(document);

            resolve(bw.generateResult(200, bw.msg.ELEMENT_REMOVED));
        });
    });
};




FragmentCommentLikeRepository.prototype.notExistsFragmentCommentLike = function() {
    let self = this;
    return function(result, callback) {
        setImmediate(function () {

            let data = {};
            data.fragment_comment_id = result._id;
            data.user_id = self.user_id;


            FragmentCommentLike.find(data, function (err, fragmentCommentLikes) {
                if (err) {
                    return callback(bw.generateResult(500, bw.msg.REMOVE_ERROR));
                }
                if (fragmentCommentLikes.length) {
                    return callback(bw.generateResult(404, bw.msg.ELEMENT_ALREADY_EXISTS));
                }

                callback(null, true);
            });
        });
    };
};


module.exports = FragmentCommentLikeRepository;
