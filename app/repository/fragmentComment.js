/**
 * Created by julian on 16/7/16.
 */

'use strict';

const FragmentComment = require('../model/fragmentComment');
const fragmentCommentType = require('../../config/assert/fragmentComment');
const Fragment = require('../model/fragment');
const Post = require('../model/post');
const User = require('../model/user');
const BaseRepository = require('./_baseRepository');
let async = require('async');

// Schema defines how the po'ts data will be stored in MongoDB
let FragmentCommentRepository = function(body, user_id, post, fragment) {
    BaseRepository.call(this, FragmentComment, fragmentCommentType);
    this.body = body;
    this.user_id = user_id || null;
    this.post = post || null;
    this.fragment = fragment || null;
    //this.validFieldsToUpdate = BaseRepository.exclusionPath(this.fields, ['__v', 'email', 'password']);
    this._init();
};

FragmentCommentRepository.prototype = Object.create(BaseRepository.prototype);
FragmentCommentRepository.prototype.constructor = FragmentCommentRepository;

FragmentCommentRepository.prototype.create = function() {
    let self = this;
    return new Promise(function(resolve, reject) {
        if (!self.checkFields({isCreating: true})) {
            reject(bw.generateResult(400, bw.msg.FILL_REQUIRED_FIELDS));
            return;
        }

        let dataExists = {};

        if (typeof self.body.parent_id !== 'undefined') {
            dataExists.parent_id = self.body.parent_id;
        }

        let data = self.getDataFromBody();

        if (data === null || self.user_id === null) {
            reject(bw.generateResult(400, bw.msg.FILL_REQUIRED_FIELDS));
        }

        /** Prevention post_id is equal to  self.post._id **/
        data.post_id = self.post._id;

        let promiseStack = [];

        promiseStack.push(self.existsParent(dataExists));
        promiseStack.push(function(result, callback) {
            setImmediate(function () {
                data.user_id = self.user_id;
                const newComment = new FragmentComment(data);

                newComment.save(function (err) {
                    if (err) {
                        return callback(bw.generateResult(500, bw.msg.CREATE_ERROR, err));
                    }

                    // Todo: it can be in the user repository like an action like increasePosts
                    User.update({'_id': self.user_id}, {'$inc': {'meta.comments': 1}}, function(err,num) {});
                    //Post.update({'_id': self.post._id}, {'$inc': {'meta.comments': 1}}, function(err,num) {});
                    Fragment.update({'_id': self.post._id}, {'$inc': {'meta.comments': 1}}, function(err,num) {});

                    if (typeof self.body.parent_id !== 'undefined') {
                        FragmentComment.update({'_id': self.body.parent_id}, {'$inc': {'meta.comments': 1}},
                        function(err,num) {});
                    }


                    callback(null, bw.generateResult(201, bw.msg.ELEMENT_CREATED));
                });
            });
        });

        /* Todo: Check if it is an open post in another case check if I am in the valid users */
        async.waterfall(promiseStack, function (err, result) {
            if(err) {
                return reject(err);
            }

            resolve(result);
        });


    });
};

FragmentCommentRepository.prototype.update = function() {
    let self = this;
    return new Promise(function(resolve, reject) {
        if (!self.checkFields({isUpdating: true})) {
            reject(bw.generateResult(400, bw.msg.FILL_REQUIRED_FIELDS));
            return;
        }

        let query = {'_id': self.body._id, user_id: self.user_id};
        let excluded = ['_id'];
        let data = self.getDataFromBody(excluded);


        if (data === null || self.user_id === null) {
            reject(bw.generateResult(400, bw.msg.FILL_REQUIRED_FIELDS));
        }

        data.updated_at = Date.now();

        FragmentComment.findOneAndUpdate(query, data, {upsert: false}, function(err, doc){
            if (!doc) {
                return reject(bw.generateResult(404, bw.msg.ELEMENT_NOT_FOUND));
            }

            if (err) {
                return reject(bw.generateResult(500, bw.msg.UPDATE_ERROR));
            }

            resolve(bw.generateResult(201, bw.msg.ELEMENT_UPDATED));
        });

    });
};

FragmentCommentRepository.prototype.existsParent = function({parent_id = null}) {
    let self = this;
    return function(callback) {
        setImmediate(function () {
            if (parent_id === null) {
                return callback(null, true);
            }

            let data = {};
            data._id = parent_id;
            data.post_id = self.post._id;
            data.fragment_id = self.fragment._id;
            data.parent_id = null;

            FragmentComment.find(data, function (err, comments) {
                if (err) {
                    return callback(bw.generateResult(500, bw.msg.SEARCH_ERROR));
                }
                if (!comments.length) {
                    return callback(bw.generateResult(404, bw.msg.ELEMENT_NOT_FOUND));
                }

                callback(null, true);
            });
        });
    };
};


module.exports = FragmentCommentRepository;
