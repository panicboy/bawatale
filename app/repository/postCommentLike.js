/**
 * Created by julian on 16/7/16.
 */

'use strict';

const PostCommentLike = require('../model/postCommentLike');
const postCommentLikeType = require('../../config/assert/postCommentLike');
const PostComment = require('../model/postComment');
//const User = require('../model/user');
const PostCommentRepository = require('../../app/repository/postComment');
const BaseRepository = require('./_baseRepository');
let async = require('async');

// Schema defines how the po'ts data will be stored in MongoDB
let PostCommentLikeRepository = function(body, user_id, post) {
    BaseRepository.call(this, PostCommentLike, postCommentLikeType);
    this.body = body;
    this.user_id = user_id || null;
    this.post = post || null;
    //this.validFieldsToUpdate = BaseRepository.exclusionPath(this.fields, ['__v', 'email', 'password']);
    this._init();
};

PostCommentLikeRepository.prototype = Object.create(BaseRepository.prototype);
PostCommentLikeRepository.prototype.constructor = PostCommentLikeRepository;

PostCommentLikeRepository.prototype.create = function() {
    let self = this;
    return new Promise(function(resolve, reject) {
        if (!self.checkFields({isCreating: true})) {
            reject(bw.generateResult(400, bw.msg.FILL_REQUIRED_FIELDS));
            return;
        }

        let data = self.getDataFromBody();

        if (data === null || self.user_id === null) {
            reject(bw.generateResult(400, bw.msg.FILL_REQUIRED_FIELDS));
        }

        /** Prevention post_id is equal to  self.post._id **/
        data.user_id = self.user_id;
        data.post_id = self.post._id;

        let promiseStack = [];

        let dataPostComment = {
            'query': {
                '_id': data.post_comment_id,
                'post_id': data.post_id
            },
            'fields': {
                '_id': 1,
                'parent_id': 1,
                'user_id': 1,
                'post_id': 1
            },
            'getOne': true
        };


        let postCommentRepository = new PostCommentRepository(null, null);

        promiseStack.push(postCommentRepository.get(dataPostComment));
        promiseStack.push(self.notExistsPostCommentLike());
        promiseStack.push(function(result, callback) {
            setImmediate(function () {
                const newLike = new PostCommentLike(data);

                newLike.save(function (err) {
                    if (err) {
                        return callback(bw.generateResult(500, bw.msg.CREATE_ERROR, err));
                    }

                    // Todo: it can be in the user repository like an action like increasePosts

                    PostComment.update(
                        {'_id': data.post_comment_id},
                        {'$inc': {'meta.likes': 1}},
                        function(err,num) {}
                    );

                    callback(null, bw.generateResult(201, bw.msg.ELEMENT_CREATED));
                });
            });
        });

        /* Todo: Check if it is an open post in another case check if I am in the valid users */
        async.waterfall(promiseStack, function (err, result) {
            if(err) {
                return reject(err);
            }

            resolve(result);
        });
    });
};


PostCommentLikeRepository.prototype.remove = function() {
    let self = this;
    return new Promise(function(resolve, reject) {
        if (!self.checkFields({isDeleting: true})) {
            reject(bw.generateResult(400, bw.msg.FILL_REQUIRED_FIELDS));
            return;
        }

        let data = self.getDataFromBody();

        if (data === null || self.user_id === null) {
            reject(bw.generateResult(400, bw.msg.FILL_REQUIRED_FIELDS));
        }

        /** Prevention post_id is equal to  self.post._id **/
        data.user_id = self.user_id;

        PostCommentLike.findOneAndRemove(data, function(err, document){
            if (err) {
                return reject(bw.generateResult(500, bw.msg.REMOVE_ERROR));
            }

            if (!document) {
                return reject(bw.generateResult(404, bw.msg.ELEMENT_NOT_FOUND));
            }

            PostComment.update(
                {'_id': document.post_comment_id},
                {'$inc': {'meta.likes': -1}},
                function(err,num) {});
            console.log(document);

            resolve(bw.generateResult(200, bw.msg.ELEMENT_REMOVED));
        });
    });
};




PostCommentLikeRepository.prototype.notExistsPostCommentLike = function() {
    let self = this;
    return function(result, callback) {
        setImmediate(function () {

            let data = {};
            data.post_comment_id = result._id;
            data.user_id = self.user_id;


            PostCommentLike.find(data, function (err, postCommentLikes) {
                if (err) {
                    return callback(bw.generateResult(500, bw.msg.SEARCH_ERROR));
                }
                if (postCommentLikes.length) {
                    return callback(bw.generateResult(404, bw.msg.ELEMENT_ALREADY_EXISTS));
                }

                callback(null, true);
            });
        });
    };
};


module.exports = PostCommentLikeRepository;
