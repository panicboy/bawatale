/**
 *
 * @constructor
 */

'use strict';

let DateStack = function() {};

/**
 *
 * @param validDate
 * @returns {string}
 */

DateStack.getFormat = function (validDate) {
  let date = (typeof validDate !== 'undefined') ? new Date(validDate) : new Date();
  let delimiter = '-';
  let dd = this.date.getDate();
  let mm = this.date.getMonth() + 1; //January is 0!

  let yyyy = date.getFullYear();
  if (dd < 10) {
    dd = '0' + dd;
  }
  if (mm < 10) {
    mm = '0' + mm;
  }

  return yyyy + delimiter + mm + delimiter + dd;
};

module.exports = DateStack;
