/**
 * Created by julian on 16/7/16.
 */

'use strict';

const PostRepository = require('../../app/repository/post');
let BwCheck = require('../../lib/utils/BwCheck');
let async = require('async');

let dataPost = {
    'query': {
        '_id': null
    },
    'fields': {
        '_id': 1,
        'user_id': 1,
        'phase': 1,
        'instant': 1,
        'configuration.private': 1,
        'configuration.private_group': 1
    },
    'getOne': true
};

let functionStack = {};

module.exports = function(fieldTocheck) {
    functionStack.grantShortByField = function (req, res, next) {

        if (typeof req.body[fieldTocheck] === 'undefined') {
            console.log('PRE POST IS NOT EXIST');
            return next();
        }

        if (!BwCheck.validate(['required', '_id'], req.body[fieldTocheck])) {
            console.log('AFTER POST IS NOT EXIST');
            return bw.send(res, 400, bw.msg.FILL_REQUIRED_FIELDS);
        }

        dataPost.query._id = req.body[fieldTocheck];
        let promiseStack = [];
        let postRepository = new PostRepository(null, null);

        promiseStack.push(postRepository.get(dataPost));
        console.log('AFTER GET EXISTS');
        /* Todo: Check if it is an open post in another case check if I am in the valid users */
        async.waterfall(promiseStack, function (err, result) {
            if(err) {
                return bw.sendResult(res, err);
            }

            req.bwpost = result;
            next();
        });
    };

    functionStack.grantByField = function (req, res, next) {

        if (typeof req.body[fieldTocheck] === 'undefined') {
            return next();
        }

        if (!BwCheck.validate(['required', '_id'], req.body[fieldTocheck])) {
            return bw.send(res, 400, bw.msg.FILL_REQUIRED_FIELDS);
        }

        dataPost.query._id = req.body[fieldTocheck];
        dataPost.fields = null;

        let promiseStack = [];
        let postRepository = new PostRepository(null, null);

        promiseStack.push(postRepository.get(dataPost));
        /* Todo: Check if it is an open post in another case check if I am in the valid users */
        async.waterfall(promiseStack, function (err, result) {
            if(err) {
                return bw.sendResult(res, err);
            }

            req.bwpost = result;
            next();
        });
    };

    return functionStack;
};
