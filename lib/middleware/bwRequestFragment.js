/**
 * Created by julian on 16/7/16.
 */

'use strict';

const FragmentRepository = require('../../app/repository/fragment');
let BwCheck = require('../../lib/utils/BwCheck');
let async = require('async');

let dataFragment = {
    'query': {
        '_id': null
    },
    'fields': {
        '_id': 1,
        'post_id': 1,
        'user_id': 1,
        'phase': 1,
        'instant': 1,
        'is_chosen': 1
    },
    'getOne': true
};

let functionStack = {};

module.exports = function(fieldToCheck) {
    functionStack.grantShortByField = function (req, res, next) {

        if (typeof req.body[fieldToCheck] === 'undefined') {
            return next();
        }

        if (!BwCheck.validate(['required', '_id'], req.body[fieldToCheck])) {
            return bw.send(res, 400, bw.msg.FILL_REQUIRED_FIELDS);
        }

        dataFragment.query._id = req.body[fieldToCheck];
        dataFragment.query.post_id = req.bwpost._id;

        let promiseStack = [];
        let fragmentRepository = new FragmentRepository(null, null);

        promiseStack.push(fragmentRepository.get(dataFragment));
        /* Todo: Check if it is an open post in another case check if I am in the valid users */
        async.waterfall(promiseStack, function (err, result) {
            if(err) {
                return bw.sendResult(res, err);
            }

            req.bwfragment = result;
            next();
        });
    };

    functionStack.grantByField = function (req, res, next) {

        if (typeof req.body[fieldToCheck] === 'undefined') {
            return next();
        }

        if (!BwCheck.validate(['required', '_id'], req.body[fieldToCheck])) {
            return bw.send(res, 400, bw.msg.FILL_REQUIRED_FIELDS);
        }

        dataFragment.query._id = req.body[fieldToCheck];
        dataFragment.query.post_id = req.bwpost._id;
        dataFragment.fields = null;

        let promiseStack = [];
        let fragmentRepository = new FragmentRepository(null, null);

        promiseStack.push(fragmentRepository.get(dataFragment));
        /* Todo: Check if it is an open post in another case check if I am in the valid users */
        async.waterfall(promiseStack, function (err, result) {
            if(err) {
                return bw.sendResult(res, err);
            }

            req.bwfragment = result;
            next();
        });
    };

    return functionStack;
};
