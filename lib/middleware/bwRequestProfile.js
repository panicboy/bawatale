/**
 * Created by julian on 16/7/16.
 */

'use strict';

const UserRepository = require('../../app/repository/user');
let BwCheck = require('../../lib/utils/BwCheck');
let async = require('async');

let dataFragment = {
    'query': {
        '_id': null
    },
    'fields': {
        '_id': 1,
        'name': 1
    },
    'getOne': true
};

let functionStack = {};

module.exports = function(fieldToCheck) {
    functionStack.grantShortByField = function (req, res, next) {

        if (typeof req.body[fieldToCheck] === 'undefined') {
            return next();
        }

        if (!BwCheck.validate(['required', '_id'], req.body[fieldToCheck])) {
            return bw.send(res, 400, bw.msg.FILL_REQUIRED_FIELDS);
        }

        dataFragment.query._id = req.body[fieldToCheck];

        let promiseStack = [];
        let userRepository = new UserRepository(null, null);

        promiseStack.push(userRepository.get(dataFragment));
        /* Todo: Check if it is an open post in another case check if I am in the valid users */
        async.waterfall(promiseStack, function (err, result) {
            if(err) {
                return bw.sendResult(res, err);
            }

            req.bwprofile = result;
            next();
        });
    };

    functionStack.grantByField = function (req, res, next) {

        if (typeof req.body[fieldToCheck] === 'undefined') {
            return next();
        }

        if (!BwCheck.validate(['required', '_id'], req.body[fieldToCheck])) {
            return bw.send(res, 400, bw.msg.FILL_REQUIRED_FIELDS);
        }

        dataFragment.query._id = req.body[fieldToCheck];
        dataFragment.fields = null;

        let promiseStack = [];
        let userRepository = new UserRepository(null, null);

        promiseStack.push(userRepository.get(dataFragment));
        /* Todo: Check if it is an open post in another case check if I am in the valid users */
        async.waterfall(promiseStack, function (err, result) {
            if(err) {
                return bw.sendResult(res, err);
            }

            req.bwprofile = result;
            next();
        });
    };

    return functionStack;
};
