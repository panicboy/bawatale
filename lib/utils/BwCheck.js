
/**
 * Class to manage the app Routes
 * @constructor
 */

'use strict';

let mongoose = require('mongoose');
let ObjectId = mongoose.Types.ObjectId;

let BwCheck = function() {};

BwCheck.validateEmail = function(value) {
  let regExp = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
  return regExp.test(value);
};

BwCheck.isShortDate = function(value) {
  let regExp = /^[0-9]{4}\-[0-9]{2}\-[0-9]{2}$/;
  return regExp.test(value);
};

BwCheck.isValidAlias = function(value) {
  let regExp = /^[a-zA-Z][0-9a-zA-Z]+$/i;
  return regExp.test(value);
};

BwCheck.isNumber = function(value) {
  let regExp = /^[0-9]+$/;
  return regExp.test(value);
};

BwCheck.validateNotRequired = function(value) {
  return typeof value === 'undefined' || value.trim() === '';
};

BwCheck.isArray = function(value) {
  return Array.isArray(value);
};

BwCheck.isString = function(value) {
  return typeof value === 'string';
};

BwCheck.validTextLength = function(value, limitLength) {
  return  BwCheck.isString(value) && value.trim() !== '' && value.length <= limitLength;
};

BwCheck.validationTypes = {

  'required': function(value) {
    return typeof value !== 'undefined' && value !== null && value.trim() !== '';
  },
  'boolean': function(value) {
    if (BwCheck.validateNotRequired(value)) {
      return true;
    }

    if (value !== '1' && value !== '0' &&
        value !== 1 && value !== 0) {
      return false;
    }

    return true;
  },
  'alias': function(value) {
    let limitLength = 30;

    if (BwCheck.validateNotRequired(value)) {
      return true;
    }

    if (!BwCheck.validTextLength(value, limitLength)) {
      return false;
    }


    return BwCheck.isValidAlias(value);
  },
  'email': function(value) {
    let limitLength = 40;

    if (BwCheck.validateNotRequired(value)) {
      return true;
    }

    if (!BwCheck.validTextLength(value, limitLength)) {
      return false;
    }


    return BwCheck.validateEmail(value);
  },
  'shortDate': function(value) {
    let limitLength = 10;

    if (BwCheck.validateNotRequired(value)) {
      return true;
    }

    if (!BwCheck.validTextLength(value, limitLength) || !BwCheck.isShortDate(value)) {
      return false;
    }

    let timestamp = Date.parse(value);

    return !isNaN(timestamp);
  },
  'smallText': function(value) {
    let limitLength = 200;

    if (BwCheck.validateNotRequired(value)) {
      return true;
    }

    return BwCheck.validTextLength(value, limitLength);
  },
  'mediumText': function(value) {
    let limitLength = 400;

    if (BwCheck.validateNotRequired(value)) {
      return true;
    }

    return BwCheck.validTextLength(value, limitLength);
  },
  'bigText': function(value) {
    let limitLength = 800;

    if (BwCheck.validateNotRequired(value)) {
      return true;
    }

    return BwCheck.validTextLength(value, limitLength);
  },
  'ultraText': function(value) {
    let limitLength = 1600;

    if (BwCheck.validateNotRequired(value)) {
      return true;
    }

    return BwCheck.validTextLength(value, limitLength);
  },
  'tinyInt': function(value) {
    let limitLength = 3;

    if (BwCheck.validateNotRequired(value)) {
      return true;
    }

    if (!BwCheck.validTextLength(value, limitLength)) {
      return false;
    }

    return BwCheck.isNumber(value);

  },
  'password': function(value) {
    let limitLength = 20;

    if (BwCheck.validateNotRequired(value)) {
      return true;
    }

    return BwCheck.validTextLength(value, limitLength);
  },
  '_id': function(value) {
    let limitLength = 30;

    if (BwCheck.validateNotRequired(value)) {
      return true;
    }

    if (!BwCheck.validTextLength(value, limitLength)) {
      return false;
    }

    return ObjectId.isValid(value);
  }
};

BwCheck.validate = function (types, value) {
  for (let i = 0; i < types.length; i++) {
    if (typeof BwCheck.validationTypes[types[i]] !== 'function' || !BwCheck.validationTypes[types[i]](value)) {
      return false;
    }
  }

  return true;
};



module.exports = BwCheck;
