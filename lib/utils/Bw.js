/**
 * Class to manage the app Routes
 * @constructor
 */
let Bw = function() {};

Bw.errorCodes = [400, 401, 403, 404, 500];

/**
 * Route method to send a valid response.
 * @param res
 * @param stCode
 * @param message
 * @param collection
 */

Bw.send = function (res, stCode, message, collection) {

  let self = this;

  let resToSend = {};
  let stCodeToSend = (typeof stCode !== 'undefined') ? stCode  : 404;
  let success = self.errorCodes.indexOf(stCodeToSend) !== -1 ? false : true;


  resToSend.st = stCodeToSend;
  resToSend.success = success;
  if (typeof message !== 'undefined' && message !== null) {
    resToSend.message = message;
  }

  if (typeof collection !== 'undefined' && collection !== null) {
    resToSend.collection = collection;
  }

  res.status(stCodeToSend);
  res.json(resToSend);
};


/**
 * Route method to send a valid json result.
 * @param res
 * @param result
 */

Bw.sendResult = function (res, result) {

  let self = this;

  result.st = (typeof result.st !== 'undefined') ? result.st  : 404;
  result.success = self.errorCodes.indexOf(result.st) !== -1 ? false : true;

  res.status(result.st);
  res.json(result);
};

/**
 * Route method to generate a valid response.
 * @param res
 * @param stCode
 * @param message
 * @param collection
 */

Bw.generateResult = function (stCode, message, collection) {

  let self = this;
  let result = {};

  result.st = (typeof stCode !== 'undefined') ? stCode  : 404;
  let success = self.errorCodes.indexOf(result.st) !== -1 ? false : true;

  result.success = success;
  if (typeof message !== 'undefined' && message !== null) {
    result.message = message;
  }

  if (typeof collection !== 'undefined' && collection !== null) {
    result.collection = collection;
  }

  return result;
};

module.exports = Bw;
