let express = require('express');
let mongoose = require ('mongoose');
let path = require('path');
let logger = require('morgan');
let bodyParser = require('body-parser');
let passport = require('passport');

let app = express();
app.disable('x-powered-by');

let router = express.Router();

// Setting global class
global.bw = require('./lib/utils/Bw');

// Setting global methods

// Setting helper classes
bw.helper = {};
bw.helper.dateStack = require('./lib/helpers/DateStack');



// Setting util classes
bw.util = {};


// passport.authenticate('jwt', { session: false });

// Setting config files
bw.cfg = {};
bw.cfg.auth = require('./config/auth');
bw.cfg.database = require('./config/database');
bw.cfg.main = require('./config/main');
bw.cfg.passport = require('./config/passport');
bw.msg = require('./config/message');




app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Initialize passport for use
app.use(passport.initialize());

mongoose.connect(bw.cfg.database.uri, bw.cfg.database.auth, function (err, db) {
  if(err) {
    console.log('['+bw.helper.dateStack.getFormat()+'] ' + bw.msg.DB_CONNECT_ERROR);
    process.exit();
  }
});

app.use('/api', require('./app/routes/controller')(app));
//app.use('/users', users);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  let err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    bw.send(res, err.status || 500, err.message, err);
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  bw.send(res, err.status || err.st || 500, bw.msg.RESPONSE_ERROR, {});
});



module.exports = app;
